# README #

Description:
------------
This is a very sample Weather application which will show the 5 days weather forecast for the City 'Moscow'
This application is making use the open web service 'http://samples.openweathermap.org/data/2.5/forecast'


Import as Project:
--------------------

1. Start Android Studio and close any open Android Studio projects.
2. From the project dialog choose the second option: Open an existing Android Studio Project
3. In the file dialog that appears, navigate into the project directory of the project (the one that contains the app folder)
4. Click Choose


How to build / run
-------------------
To build and run your app, select Run > Run in the menu bar (or click Run  in the toolbar)

What could be done with more time
---------------------------------
1. We can add good images so that application look and feel can be improve
2. We can add more functionalities like choosing different CITY for the weather forecast




