package com.weather.test.weather;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.GregorianCalendar;
/**
 * Created by KA311713 on 6/19/2017.
 */

public class CustomListAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList data;
    private static LayoutInflater inflater=null;
    public Resources res;
    ForecastInfo tempValues=null;
    int i=0;

    /*************  CustomAdapter Constructor *****************/
    public CustomListAdapter(Activity a, ArrayList<ForecastInfo> d) {

        /********** Take passed values **********/
        activity = a;
        data=d;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = ( LayoutInflater )activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /******** What is the size of Passed Arraylist Size ************/
    public int getCount() {

        if(data.size()<=0)
            return 1;
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView desc;
        public TextView humidity;
        public TextView temp_min;
        public TextView temp_max;
        public TextView dateLabel;
    }

    /****** Depends upon data size called for each row , Create each ListView row *****/
    public View getView(int position, View convertView, ViewGroup parent) {
        GregorianCalendar gc = new GregorianCalendar();
        View vi = convertView;
        ViewHolder holder;

        if(convertView==null){

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.forecast_item, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.desc = (TextView) vi.findViewById(R.id.desc);
            holder.humidity=(TextView)vi.findViewById(R.id.humidity);
            holder.temp_min = (TextView) vi.findViewById(R.id.temp_min);
            holder.temp_max=(TextView)vi.findViewById(R.id.temp_max);
            holder.dateLabel=(TextView)vi.findViewById(R.id.dateLabel);
            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        }
        else
            holder=(ViewHolder)vi.getTag();


            /***** Get each Model object from Arraylist ********/
            tempValues=null;
            tempValues = ( ForecastInfo ) data.get( position );

            /************  Set Model values in Holder elements ***********/

            holder.desc.setText( tempValues.getDescription() );
            holder.humidity.setText( tempValues.getHumidity() );
        holder.temp_min.setText( tempValues.getTemp_min());
        holder.temp_max.setText( tempValues.getTemp_max() );
            if(position ==0){
                holder.dateLabel.setText( "Today" );}
            else {
                gc.add(Calendar.DATE, position);
                holder.dateLabel.setText( gc.getTime()+"");
            }
        return vi;
    }


}
