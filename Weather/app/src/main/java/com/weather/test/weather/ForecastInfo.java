package com.weather.test.weather;

/**
 * Created by KA311713 on 6/19/2017.
 */

public class ForecastInfo {
    String humidity;
    String temp_min;
    String description;
    String temp_max;

    public ForecastInfo(String humidity, String temp_min, String description, String temp_max) {
        this.humidity = humidity;
        this.temp_min = temp_min;
        this.description = description;
        this.temp_max = temp_max;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(String temp_min) {
        this.temp_min = temp_min;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(String temp_max) {
        this.temp_max = temp_max;
    }
}
