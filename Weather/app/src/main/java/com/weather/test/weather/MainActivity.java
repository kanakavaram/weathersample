package com.weather.test.weather;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;

public class MainActivity extends AppCompatActivity {
    private String TAG = MainActivity.class.getName();
    private ProgressDialog p;
    HashSet<String> tempSet;
    ArrayList<ForecastInfo> forecastArray;
    ListView mListView;
    CustomListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        p=new ProgressDialog(MainActivity.this);
        forecastArray= new ArrayList<ForecastInfo>();
        tempSet=new HashSet<>();
        mListView=(ListView) findViewById(R.id.forecast_list);
        Forecast fc= new Forecast();
        fc.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class Forecast extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            p.setMessage("Getting data");
            p.setIndeterminate(false);
            p.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            p.setCancelable(false);
            p.show();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                URL url=new URL("http://samples.openweathermap.org/data/2.5/forecast?id=524901&appid=b1b15e88fa797225412429c1c50c122a1");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                InputStream in = new BufferedInputStream(conn.getInputStream());
                String response = getStringFromInputStream(in);
                System.out.println(response);
                JSONObject result = new JSONObject(response);
                if(null != result){
                    JSONArray forecastList= result.getJSONArray("list");
                    for(int i=0;i<forecastList.length();i++){
                        JSONObject item = (JSONObject)forecastList.get(i);
                        String dateStr=item.getString("dt_txt");
                        if(tempSet.add(dateStr.split(" ")[0])){
                            JSONObject main  = item.getJSONObject("main");
                            JSONArray weather  = item.getJSONArray("weather");
                            ForecastInfo obj=new ForecastInfo(main.getString("humidity"),main.getString("temp_min"),((JSONObject)weather.get(0)).getString("description"),main.getString("temp_max"));
                            forecastArray.add(obj);
                        }
                    }
                }
            } catch (Exception e) {
                Log.e(TAG,e.getMessage());
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(null !=forecastArray && forecastArray.size() >0){
                adapter=new CustomListAdapter( MainActivity.this, forecastArray );
                mListView.setAdapter( adapter );
            }
            p.dismiss();
        }
    }

    private String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }
}
